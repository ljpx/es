package es

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/ljpx/id"
)

func TestMemoryEventStoreGetByIDNoStream(t *testing.T) {
	// Arrange.
	mes := NewMemoryEventStore(nil)
	streamID := id.New()
	aggregate := &testAggregate{}

	// Act.
	err := mes.GetByID(streamID, aggregate)

	// Assert.
	require.NotNil(t, err)
	require.Equal(t, fmt.Sprintf("event stream with ID '%v' was not found", streamID), err.Error())
}

func TestMemoryEventStoreSymmetry(t *testing.T) {
	// Arrange.
	mss := NewMemoryStateStore()
	mes := NewMemoryEventStore(mss)
	aggregate1 := newtestAggregate("James", "Brown")

	// Act.
	err := mes.Save(aggregate1)
	require.Nil(t, err)

	aggregate2 := &testAggregate{}
	err = mes.GetByID(aggregate1.StreamID, aggregate2)
	require.Nil(t, err)

	// Assert.
	require.Equal(t, "James", aggregate2.FirstName)
	require.Equal(t, "Brown", aggregate2.LastName)

	state := &testState{}
	err = mss.Retrieve(state, aggregate2.GetStreamID())
	require.Nil(t, err)

	require.Equal(t, "James Brown", state.FullName)
}
