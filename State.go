package es

import (
	"gitlab.com/ljpx/id"
	"gitlab.com/ljpx/trace"
)

// State defines the methods that any state must implement.  A state is a small
// scale definition of the current state of an aggregate.
type State interface {
	trace.Identifiable

	GetStreamID() id.ID
	SetStreamID(streamID id.ID)
}
