package es

import (
	"fmt"

	"gitlab.com/ljpx/id"
)

// MemoryEventStore is an implementation of EventStore in RAM.  It should only
// ever be used for testing.  It is not thread safe.
type MemoryEventStore struct {
	events     map[id.ID][]Event
	stateStore StateStore
}

var _ EventStore = &MemoryEventStore{}

// NewMemoryEventStore creates a new in-memory event store.
func NewMemoryEventStore(stateStore StateStore) *MemoryEventStore {
	return &MemoryEventStore{
		events:     make(map[id.ID][]Event),
		stateStore: stateStore,
	}
}

// GetByID returns the event stream specified by the stream ID.
func (es *MemoryEventStore) GetByID(streamID id.ID, aggregate Aggregate) error {
	eventStream, ok := es.events[streamID]
	if !ok {
		return fmt.Errorf("event stream with ID '%v' was not found", streamID)
	}

	aggregate.SetStreamID(streamID)

	for _, event := range eventStream {
		Apply(aggregate, event)
	}

	return nil
}

// Save saves the uncommitted events of the aggregate to the stream it is
// associated with.
func (es *MemoryEventStore) Save(aggregate Aggregate) error {
	streamID := aggregate.GetStreamID()
	eventStream, _ := es.events[streamID]

	for _, event := range aggregate.GetUncommittedEvents() {
		eventStream = append(eventStream, event)
	}

	es.events[streamID] = eventStream
	aggregate.ClearUncommittedEvents()

	return es.stateStore.Store(aggregate.State())
}
