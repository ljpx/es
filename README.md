![](icon.png)

# es

Package `es` provides a number of primitives used in event sourcing.

## Usage Example

Consider the set of files below.

`./aggregate/User.go`

```go
package aggregate

import (
    "estst/event"
    "estst/state"

    "gitlab.com/ljpx/es"
    "gitlab.com/ljpx/id"
)

// User is an aggregate.
type User struct {
    es.AggregateBase

    FirstName string
    LastName  string
}

var _ es.Aggregate = &User{}

// NewUser creates a new user.
func NewUser(firstName string, lastName string) *User {
    a := &User{}
    a.SetStreamID(id.New())

    e := &event.UserCreated{
        UserID:    a.GetStreamID(),
        FirstName: firstName,
        LastName:  lastName,
    }

    es.Apply(a, e)
    es.Track(a, e)

    return a
}

// ApplyUserCreatedEvent is an apply method.
func (a *User) ApplyUserCreatedEvent(e *event.UserCreated) {
    a.FirstName = e.FirstName
    a.LastName = e.LastName
}

// Identify identifies the aggregate.
func (*User) Identify() string {
    return "User"
}

// State returns the aggregate state.
func (a *User) State() es.State {
    state := &state.User{}
    state.SetStreamID(a.GetStreamID())

    state.FirstName = a.FirstName
    state.LastName = a.LastName

    return state
}
```

`./state/User.go`

```go
package state

import "gitlab.com/ljpx/es"

// User represents the computed state of the User aggregate at a moment in time.
type User struct {
    es.StateBase

    FirstName string `json:"firstName"`
    LastName  string `json:"lastName"`
}

var _ es.State = &User{}

// Identify identifies the state.
func (*User) Identify() string {
    return "User"
}
```

`./event/UserCreated.go`

```go
package event

import (
    "fmt"

    "gitlab.com/ljpx/es"
    "gitlab.com/ljpx/id"
)

// UserCreated is raised when a user is first created.
type UserCreated struct {
    es.EventBase

    UserID    id.ID  `json:"userID"`
    FirstName string `json:"firstName"`
    LastName  string `json:"lastName"`
}

var _ es.Event = &UserCreated{}

// Identify identifies the event.
func (*UserCreated) Identify() string {
    return "UserCreatedEvent"
}

// Describe describes the contents of the event.
func (e *UserCreated) Describe() string {
    return fmt.Sprintf("The creation of the user '%v %v' (UserID: %v)", e.FirstName, e.LastName, e.UserID)
}

// Redact produces a copy of the event without sensitive information.
func (e *UserCreated) Redact() interface{} {
    cp := *e
    return cp
}
```
