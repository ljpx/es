package es

import (
	"fmt"
	"reflect"

	"gitlab.com/ljpx/id"
)

// AggregateBase implements Aggregate and can be easily embedded into new event
// types.
type AggregateBase struct {
	StreamID              id.ID
	CurrentSequenceNumber int
	UncommittedEvents     []Event
}

// GetStreamID returns the ID of the stream.
func (a *AggregateBase) GetStreamID() id.ID {
	return a.StreamID
}

// SetStreamID sets the ID of the stream.
func (a *AggregateBase) SetStreamID(streamID id.ID) {
	a.StreamID = streamID
}

// GetCurrentSequenceNumber returns the current sequence number.
func (a *AggregateBase) GetCurrentSequenceNumber() int {
	return a.CurrentSequenceNumber
}

// SetCurrentSequenceNumber sets the current sequence number.
func (a *AggregateBase) SetCurrentSequenceNumber(currentSequenceNumber int) {
	a.CurrentSequenceNumber = currentSequenceNumber
}

// GetUncommittedEvents returns the list of uncommitted events for the
// aggregate.
func (a *AggregateBase) GetUncommittedEvents() []Event {
	return a.UncommittedEvents
}

// AddUncommittedEvent adds an event to the list of uncommitted events for the
// aggregate.
func (a *AggregateBase) AddUncommittedEvent(event Event) {
	a.UncommittedEvents = append(a.UncommittedEvents, event)
}

// ClearUncommittedEvents clears the list of uncommitted events for the
// aggregate.
func (a *AggregateBase) ClearUncommittedEvents() {
	a.UncommittedEvents = nil
}

// Apply applies the provided event to the provided aggregate.
func Apply(aggregate Aggregate, event Event) {
	methodName := fmt.Sprintf("Apply%v", event.Identify())

	applyMethod := reflect.ValueOf(aggregate).MethodByName(methodName)
	if !applyMethod.IsValid() {
		aggregateType := reflect.TypeOf(aggregate)
		eventType := reflect.TypeOf(event)
		panicMsgTemplate := "attempted apply a '%v' to a '%v' using the '%v' method, but the method did not exist"
		panicMsg := fmt.Sprintf(panicMsgTemplate, eventType, aggregateType, methodName)
		panic(panicMsg)
	}

	applyMethod.Call([]reflect.Value{reflect.ValueOf(event)})

	currentSequence := aggregate.GetCurrentSequenceNumber()
	aggregate.SetCurrentSequenceNumber(currentSequence + 1)
}

// Track adds the given event to the list of uncommitted events for the
// aggregate, whilst also adding the event, stream ID, and sequence number to
// the event itself.
func Track(aggregate Aggregate, event Event) {
	event.SetEventID(id.New())
	event.SetStreamID(aggregate.GetStreamID())
	event.SetSequenceNumber(aggregate.GetCurrentSequenceNumber())

	aggregate.AddUncommittedEvent(event)
}
