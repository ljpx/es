package es

import (
	"gitlab.com/ljpx/id"
	"gitlab.com/ljpx/trace"
)

// Event defines the methods that any event must implement.
type Event interface {
	trace.Traceable

	GetEventID() id.ID
	SetEventID(eventID id.ID)

	GetStreamID() id.ID
	SetStreamID(streamID id.ID)

	GetSequenceNumber() int
	SetSequenceNumber(sequenceNumber int)
}
