package es

import "gitlab.com/ljpx/id"

// StateBase implements State and can be easily embedded into new aggregate
// state types.
type StateBase struct {
	StreamID id.ID
}

// GetStreamID returns the ID of the stream.
func (s *StateBase) GetStreamID() id.ID {
	return s.StreamID
}

// SetStreamID sets the ID of the stream.
func (s *StateBase) SetStreamID(streamID id.ID) {
	s.StreamID = streamID
}
