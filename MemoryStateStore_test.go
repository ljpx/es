package es

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/ljpx/id"
)

func TestMemoryStateStoreSymmetric(t *testing.T) {
	// Arrange.
	mss := NewMemoryStateStore()
	state1 := &testState{FullName: "John Smith"}
	state1.SetStreamID(id.New())

	// Act.
	err := mss.Store(state1)
	require.Nil(t, err)

	state2 := &testState{}
	err = mss.Retrieve(state2, state1.StreamID)
	require.Nil(t, err)

	// Assert.
	require.Equal(t, state1, state2)
}

func TestMemoryStateUnknownID(t *testing.T) {
	// Arrange.
	mss := NewMemoryStateStore()
	streamID := id.New()
	state := &testState{}

	// Act.
	err := mss.Retrieve(state, streamID)

	// Assert.
	require.NotNil(t, err)
	require.Equal(t, fmt.Sprintf("the 'test' with streamID '%v' was not found in the state store", streamID), err.Error())
}
