package es

import "gitlab.com/ljpx/id"

// StateStore defines the methods that any state store must implement.  A state
// store is used to store states for later querying and use.
type StateStore interface {
	Store(state State) error
	Retrieve(state State, streamID id.ID) error
}
