package es

import "gitlab.com/ljpx/id"

// EventStore defines the methods that any event storage service must implement.
type EventStore interface {
	GetByID(streamID id.ID, aggregate Aggregate) error
	Save(aggregate Aggregate) error
}
