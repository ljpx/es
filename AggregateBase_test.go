package es

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/ljpx/id"
)

func TestAggregateBaseGetSetStreamID(t *testing.T) {
	// Arrange.
	a := &AggregateBase{}
	streamID1 := id.New()

	// Act.
	a.SetStreamID(streamID1)
	streamID2 := a.GetStreamID()

	// Assert.
	require.Equal(t, streamID1, streamID2)
}

func TestAggregateBaseGetSetCurrentSequenceNumber(t *testing.T) {
	// Arrange.
	a := &AggregateBase{}

	// Act.
	a.SetCurrentSequenceNumber(42)
	currentSequenceNumber := a.GetCurrentSequenceNumber()

	// Assert.
	require.Equal(t, 42, currentSequenceNumber)
}

func TestAggregateBaseAddGetUncommittedEvents(t *testing.T) {
	// Arrange.
	a := &AggregateBase{}
	event := &testAggregateEvent{}

	// Precondition.
	require.Empty(t, a.GetUncommittedEvents())

	// Act.
	a.AddUncommittedEvent(event)

	// Assert.
	uncommittedEvents := a.GetUncommittedEvents()
	require.Len(t, uncommittedEvents, 1)
	require.Exactly(t, event, uncommittedEvents[0])
}

func TestAggregateBaseClearUncommittedEvents(t *testing.T) {
	// Arrange.
	a := &AggregateBase{}
	event := &testAggregateEvent{}
	a.AddUncommittedEvent(event)

	// Precondition.
	require.Len(t, a.GetUncommittedEvents(), 1)

	// Act.
	a.ClearUncommittedEvents()

	// Assert.
	require.Empty(t, a.GetUncommittedEvents())
}

func TestAggregateBaseApplySuccess(t *testing.T) {
	// Arrange and Act.
	a := newtestAggregate("John", "Smith")

	// Assert.
	require.Equal(t, "John", a.FirstName)
	require.Equal(t, "Smith", a.LastName)
	require.Equal(t, 1, a.GetCurrentSequenceNumber())
	require.Len(t, a.GetUncommittedEvents(), 1)
	require.True(t, a.GetStreamID().IsValid())
}

func TestAggregateBaseState(t *testing.T) {
	// Arrange and Act.
	a := newtestAggregate("John", "Smith")
	state := a.State().(*testState)

	// Assert.
	require.Equal(t, "John Smith", state.FullName)
}

func TestAggregateBaseApplyFailure(t *testing.T) {
	// Arrange.
	a := &testAggregate{}

	// Act.
	expectedPanicMessage := "attempted apply a '*es.testUnknownEvent' to a '*es.testAggregate' using the 'ApplytestUnknownEvent' method, but the method did not exist"
	require.PanicsWithValue(t, expectedPanicMessage, func() {
		Apply(a, &testUnknownEvent{})
	})
}

// -----------------------------------------------------------------------------

type testAggregate struct {
	AggregateBase

	FirstName string
	LastName  string
}

var _ Aggregate = &testAggregate{}

func newtestAggregate(firstName string, lastName string) *testAggregate {
	aggregate := &testAggregate{}
	aggregate.SetStreamID(id.New())

	event := &testAggregateEvent{
		FirstName: firstName,
		LastName:  lastName,
	}

	Apply(aggregate, event)
	Track(aggregate, event)

	return aggregate
}

func (a *testAggregate) ApplytestAggregateEvent(event *testAggregateEvent) {
	a.FirstName = event.FirstName
	a.LastName = event.LastName
}

func (a *testAggregate) Identify() string {
	return "test"
}

func (a *testAggregate) State() State {
	state := &testState{
		FullName: a.FirstName + " " + a.LastName,
	}

	state.SetStreamID(a.GetStreamID())
	return state
}

type testState struct {
	StateBase

	FullName string
}

var _ State = &testState{}

func (*testState) Identify() string {
	return "test"
}

type testAggregateEvent struct {
	EventBase

	FirstName string
	LastName  string
}

var _ Event = &testAggregateEvent{}

func (*testAggregateEvent) Identify() string {
	return "testAggregateEvent"
}

func (e *testAggregateEvent) Describe() string {
	return fmt.Sprintf("testAggregateEvent with Name '%v %v'", e.FirstName, e.LastName)
}

func (e *testAggregateEvent) Redact() interface{} {
	cp := *e
	return cp
}

type testUnknownEvent struct {
	EventBase
}

var _ Event = &testUnknownEvent{}

func (*testUnknownEvent) Identify() string {
	return "testUnknownEvent"
}

func (e *testUnknownEvent) Describe() string {
	return "testUnknownEvent"
}

func (e *testUnknownEvent) Redact() interface{} {
	cp := *e
	return cp
}
