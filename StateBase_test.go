package es

import (
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/ljpx/id"
)

func TestStateBaseGetSetStreamID(t *testing.T) {
	// Arrange.
	s := &StateBase{}
	streamID1 := id.New()

	// Act.
	s.SetStreamID(streamID1)
	streamID2 := s.GetStreamID()

	// Assert.
	require.Equal(t, streamID1, streamID2)
}
