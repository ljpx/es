package es

import (
	"encoding/json"
	"fmt"

	"gitlab.com/ljpx/id"
)

// MemoryStateStore is an implementation of StateStore in RAM.  It should only
// ever be used for testing.  It is not thread safe.
type MemoryStateStore struct {
	states map[string]map[id.ID][]byte
}

var _ StateStore = &MemoryStateStore{}

// NewMemoryStateStore creates a new memory state store.
func NewMemoryStateStore() *MemoryStateStore {
	return &MemoryStateStore{
		states: make(map[string]map[id.ID][]byte),
	}
}

// Store stores the state in the state store.
func (m *MemoryStateStore) Store(state State) error {
	streamName := state.Identify()
	streamID := state.GetStreamID()
	m.ensureStreamNameExists(streamName)

	rawJSON, err := json.Marshal(state)
	if err != nil {
		return err
	}

	m.states[streamName][streamID] = rawJSON
	return nil
}

// Retrieve retrieves the state from the state store.
func (m *MemoryStateStore) Retrieve(state State, streamID id.ID) error {
	streamName := state.Identify()
	m.ensureStreamNameExists(streamName)

	rawJSON, ok := m.states[streamName][streamID]
	if !ok {
		return fmt.Errorf("the '%v' with streamID '%v' was not found in the state store", streamName, streamID)
	}

	return json.Unmarshal(rawJSON, state)
}

func (m *MemoryStateStore) ensureStreamNameExists(streamName string) {
	_, ok := m.states[streamName]
	if !ok {
		m.states[streamName] = make(map[id.ID][]byte)
	}
}
