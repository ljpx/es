package es

import (
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/ljpx/id"
)

func TestEventBaseGetSetEventID(t *testing.T) {
	// Arrange.
	e := &EventBase{}
	eventID1 := id.New()

	// Act.
	e.SetEventID(eventID1)
	eventID2 := e.GetEventID()

	// Assert.
	require.Equal(t, eventID1, eventID2)
}

func TestEventBaseSetEventIDSetsCorrelationID(t *testing.T) {
	// Arrange.
	e := &EventBase{}
	eventID1 := id.New()
	eventID2 := id.New()

	// Act.
	e.SetEventID(eventID1)
	e.SetEventID(eventID2)

	// Assert.
	eventID3, ok := e.GetCorrelation("event")
	require.True(t, ok)
	require.Equal(t, eventID2, eventID3)
}

func TestEventBaseGetSetStreamID(t *testing.T) {
	// Arrange.
	e := &EventBase{}
	streamID1 := id.New()

	// Act.
	e.SetStreamID(streamID1)
	streamID2 := e.GetStreamID()

	// Assert.
	require.Equal(t, streamID1, streamID2)
}

func TestEventBaseSetStreamIDSetsCorrelationID(t *testing.T) {
	// Arrange.
	e := &EventBase{}
	streamID1 := id.New()
	streamID2 := id.New()

	// Act.
	e.SetStreamID(streamID1)
	e.SetStreamID(streamID2)

	// Assert.
	streamID3, ok := e.GetCorrelation("stream")
	require.True(t, ok)
	require.Equal(t, streamID2, streamID3)
}

func TestEventBaseGetSetSequenceNumber(t *testing.T) {
	// Arrange.
	e := &EventBase{}

	// Act.
	e.SetSequenceNumber(42)
	sequenceNumber := e.GetSequenceNumber()

	// Assert.
	require.Equal(t, 42, sequenceNumber)
}
