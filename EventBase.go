package es

import (
	"gitlab.com/ljpx/id"
	"gitlab.com/ljpx/trace"
)

// EventBase implements (most of) Event and can be easily embedded into new
// event types.
type EventBase struct {
	trace.CorrelatableBase

	EventID        id.ID
	StreamID       id.ID
	SequenceNumber int
}

// GetEventID returns the ID of the event.
func (e *EventBase) GetEventID() id.ID {
	return e.EventID
}

// SetEventID sets the ID of the event, ensuring it is also present in the set
// of correlation IDs.
func (e *EventBase) SetEventID(eventID id.ID) {
	e.AddCorrelation("event", eventID)
	e.EventID = eventID
}

// GetStreamID returns the ID of the stream that the event belongs to.
func (e *EventBase) GetStreamID() id.ID {
	return e.StreamID
}

// SetStreamID sets the ID of the stream that the event belongs to, ensuring it
// is also present in the set of correlation IDs.
func (e *EventBase) SetStreamID(streamID id.ID) {
	e.AddCorrelation("stream", streamID)
	e.StreamID = streamID
}

// GetSequenceNumber gets the index of the event in the stream.
func (e *EventBase) GetSequenceNumber() int {
	return e.SequenceNumber
}

// SetSequenceNumber sets the index of the event in the stream.
func (e *EventBase) SetSequenceNumber(sequenceNumber int) {
	e.SequenceNumber = sequenceNumber
}
