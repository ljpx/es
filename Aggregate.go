package es

import (
	"gitlab.com/ljpx/id"
	"gitlab.com/ljpx/trace"
)

// Aggregate defines the methods that any aggregate must implement.  An
// aggregate defines how a stream of events shapes the state of something over
// time.
type Aggregate interface {
	trace.Identifiable

	GetStreamID() id.ID
	SetStreamID(streamID id.ID)

	GetCurrentSequenceNumber() int
	SetCurrentSequenceNumber(currentSequenceNumber int)

	GetUncommittedEvents() []Event
	AddUncommittedEvent(event Event)
	ClearUncommittedEvents()

	State() State
}
